import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

import CharacterList from './components/CharacterList';
import CharacterView from './components/CharacterView';

import './App.css';

class App extends React.Component {
  render() {
    return (
      <Router>
        <div className="container" >
          <nav className="navbar">
            <ul className="nav navbar-nav">
              <li>
                <Link to="/">Characters</Link>
              </li>
            </ul>
          </nav>

          <Switch>
            <Route path="/character/:id">
              <CharacterView />
            </Route>
            <Route path="/">
              <CharacterList />
            </Route>
          </Switch>
        </div>
      </Router>
    );
  }
}

export default App;
