import React from 'react';
import {
  Link
} from "react-router-dom";

function CharacterEl(props) {
	const character = props.character;
    return (
		<tr>
			<td>
				<Link to={location => `/character/${character.id}`} >{character.name}</Link>
			</td>
			<td>{character.slug}</td>
			<td>{character.gender}</td>
			<td>{character.house}</td>
			<td>{character.rank}</td>
		</tr>
    );

}

export default CharacterEl;