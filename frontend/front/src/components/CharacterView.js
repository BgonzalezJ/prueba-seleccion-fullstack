import React from 'react';
import { withRouter } from "react-router";

import { getCharacterById } from '../lib/Api';

class CharacterView extends React.Component {

  state = {
  	character: {
  		books: [],
  		titles: []
  	}
  }

  componentDidMount() {
  	const id = this.props.match.params.id;
  	getCharacterById(id).then( res => {
  		const character = res.data;
      	this.setState({character});
  	});
  }

  render() {
  	
  	const character = this.state.character;

	if (!character.image)
		character.image = process.env.PUBLIC_URL + '/no-image.png';

	if (!character.gender)
		character.gender = "Not defined";

    return (
    	<div className="container">
        <div className="row">
            <div className="col-12">
              <header>
                <h1>{ character.name }</h1>
              </header>
            </div>

            <div className="col-12 col-lg-4">
            	<img src={ character.image } alt="Charater" />
            </div>
            <div className="col-12 col-lg-8">
            	<table className="table">
            		<tbody>
	            		<tr>
	            			<td>Name: </td>
	            			<td>{character.name}</td>
	            		</tr>
	            		<tr>
	            			<td>Slug: </td>
	            			<td>{character.slug}</td>
	            		</tr>
	            		<tr>
	            			<td>Gender: </td>
	            			<td>{character.gender}</td>
	            		</tr>
	            		<tr>
	            			<td>House: </td>
	            			<td>{character.house}</td>
	            		</tr>
	            		<tr>
	            			<td>Rank: </td>
	            			<td>{character.rank}</td>
	            		</tr>
	            		<tr>
	            			<td>Books: </td>
	            			<td>
	            				<ul>
	            					{character.books.map((book, index) => ( <li key={index}>{book.name}</li>))}
	            				</ul>
	            			</td>
	            		</tr>
	            		<tr>
	            			<td>Titles: </td>
	            			<td>
	            				<ul>
	            					{character.titles.map((title, index) => ( <li key={index}>{title.name}</li>))}
	            				</ul>
	            			</td>
	            		</tr>
            		</tbody>
            	</table>
            </div>

        </div>
      </div>
    );
  }
}

export default withRouter(CharacterView);