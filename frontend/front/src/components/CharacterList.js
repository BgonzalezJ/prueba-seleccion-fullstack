import React from 'react';
import CharacterEl from './CharacterEl';

import { getCharacters } from '../lib/Api';

class CharacterList extends React.Component {

  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.search       = this.search.bind(this);
    this.changePage   = this.changePage.bind(this);
  }

  state = {
    characters: [],
    page: 1,
    s: "",
    disableNextButton: false
  }

  componentDidMount() {
    this.getCharactersFromApi();
  }

  getCharactersFromApi() {
    const page  = this.state.page;
    const s     = this.state.s;
    
    getCharacters(page, s).then(res => {
      const characters = res.data;
      this.setState({characters});
      if (res.data.length > 0) {
        this.setState({disableNextButton: false});
      } else {
        this.setState({disableNextButton: true});
      }
    }); 
  }

  handleChange(event) {
    this.setState({s: event.target.value});
  }

  search() {
    this.setState({page: 1}, this.getCharactersFromApi);
  }

  changePage(type) {
    let page = this.state.page;
    if (type == "next")
      page++;
    else {
      if (page > 1)
        page--;
    }
    this.setState({page: page}, this.getCharactersFromApi);
  }

  render() {
    const characters = this.state.characters;
    return (
    	<div className="container">
        <div className="row">
            <div className="col-12">

              <header>
                <h1>List of Characters</h1>
              </header>

              <div className="search-wrapper">
                  <input type="text" placeholder="Search by character's name or house" value={this.state.s} onChange={this.handleChange} />
                  <button onClick={this.search} >Search</button>
              </div>

              <div className="pagination-wrapper">
                <button onClick={ () => this.changePage('previous')} disabled={ this.state.page == 1 }>Previous</button>
                <button onClick={ () => this.changePage('next')} disabled={this.state.disableNextButton} >Next</button>
              </div>

              <table className="table">
                <thead>
                  <tr>
                    <th>Name</th>
                    <th>Slug</th>
                    <th>Gender</th>
                    <th>House</th>
                    <th>Rank</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>Name</th>
                    <th>Slug</th>
                    <th>Gender</th>
                    <th>House</th>
                    <th>Rank</th>
                  </tr>
                </tfoot>
                <tbody>
                {characters.map((character, index) => (<CharacterEl key={index} character={character} />))}
                </tbody>
              </table>
            </div>
        </div>
      </div>
    );
  }
}

export default CharacterList;