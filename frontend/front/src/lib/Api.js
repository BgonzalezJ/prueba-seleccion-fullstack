import axios from 'axios';

const API_URL = 'http://localhost:8000';

export const getCharacters = (page, s) => {
	return axios.get(API_URL + "/characters?page=" + page + "&s=" + s)
}

export const getCharacterById = (id) => {
	return axios.get(API_URL + "/characters/" + id)
}