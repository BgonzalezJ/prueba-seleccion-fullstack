module.exports = (sql, Sequelize) => {
	return sql.define('character', {
		id: {
			type: Sequelize.INTEGER,
			primaryKey: true,
			autoIncrement: true
		},
		name: {
			type: Sequelize.STRING
		},
		slug: {
			type: Sequelize.STRING
		},
		image: {
			type: Sequelize.STRING
		},
		gender: {
			type: Sequelize.STRING
		},
		house: {
			type: Sequelize.STRING
		},
		rank: {
			type: Sequelize.INTEGER
		},
	})
}