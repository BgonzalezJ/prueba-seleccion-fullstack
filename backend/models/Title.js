module.exports = (sql, Sequelize) => {
	return sql.define('title', {
		id: {
			type: Sequelize.INTEGER,
			primaryKey: true,
			autoIncrement: true
		},
		characterId: {
			type: Sequelize.INTEGER,
		},
		name: {
			type: Sequelize.STRING
		}
	})
}