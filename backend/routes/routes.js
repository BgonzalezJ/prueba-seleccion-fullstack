const {Op} = require('sequelize');

const router = (app, bodyParser, Character, Book, Title) => {

    app.use(function(req, res, next) {
      res.header("Access-Control-Allow-Origin", "*");
      res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
      next();
    });

	app.use(bodyParser.json());
	app.use(bodyParser.urlencoded({
	    extended: true,
	}));

    app.get('/characters', (req, res) => {
    	const page = req.query.page ? req.query.page - 1 : 0;
    	const limit = 10;
    	const offset = limit * page;
    	const keyword = req.query.s;

        console.log(keyword);

    	let options = {
    		limit: limit,
    		offset: offset
    	}
    	
    	if (keyword) {
    		options.where = {
    			[Op.or] : [{name: {[Op.substring]: req.query.s}}, {house: {[Op.substring]: req.query.s}}]
    		}
    	}
    	
    	Character.findAll(options).then(characters => res.send(characters))
    });

    app.get('/characters/:id', (req, res) => {
    	Character.findByPk(req.params.id, {
    		include: [
                {model: Book, as: Book.tableName},
                {model: Title, as: Title.tableName}
            ]
    	}).then(character => res.send(character))
    });
}

module.exports = router;