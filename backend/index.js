require('dotenv').config();

const https 		= require("https");
const express 		= require("express");
const bodyParser 	= require("body-parser");
const routes 		= require('./routes/routes');
const app 			= express();
const Sql			= require('sequelize');

const CharacterModel    = require('./models/Character');
const BookModel    		= require('./models/Book');
const TitleModel    	= require('./models/Title');

const sequelize = new Sql(process.env.DATABASE, process.env.DB_USER, process.env.PASSWORD, {
	host: process.env.HOST,
	dialect: process.env.DRIVER
});

const Character = CharacterModel(sequelize, Sql);
const Book 		= BookModel(sequelize, Sql);
const Title 	= TitleModel(sequelize, Sql);

Character.hasMany(Book);
Character.hasMany(Title);

Book.belongsTo(Character);
Title.belongsTo(Character);



const options = {
	host: 'api.got.show',
  	port: 443,
  	path: '/api/general/characters',
  	method: 'GET',
  	headers: {
	    'Content-Type': 'application/json',
  	}
}

https.request(options, (res) => {
	let chunks = [];
	res.on('data', (data) => {
		chunks.push(data);
  	});

  	res.on('end', () => {
  		let data   = Buffer.concat(chunks);
  		let schema = JSON.parse(data);
  		schema = schema.book.map(e => {return {name: e.name, slug: e.slug, image: e.image, books: e.books, titles: e.titles, gender: e.gender, rank: e.pagerank.rank, house: e.house}});
  		schema.forEach(d => {
  			Character.create(d)
  			.then(
  				character => {
  					const books = d.books.map(b => {return {characterId: character.id, name: b}})
  					const titles = d.titles.map(t => {return {characterId: character.id, name: t}})

  					books.forEach(book => {
  						Book.create(book);
  					});

  					titles.forEach(title => {
  						Title.create(title);
  					});
  				})
  		});
  	});
}).end();

// Definimos Rutas
routes(app, bodyParser, Character, Book, Title);

app.listen(process.env.PORT, () => {
 console.log(`El servidor está inicializado en el puerto ${process.env.PORT}`);
});